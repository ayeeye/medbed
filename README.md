# MedBed

MedBed is a Django based application, scheduled to be released in mid December 2020. This application was made to fulfill the final project of the AI Fasilkom UI course.

The use of this application is to search nearest covid referral hospitals in Jakarta. Also provided room level for user preference.
## Installation

It is recommended to use the python environment to install this application.
The requirements can be installed by

```bash
pip install -r requirements.txt
```
This application use osm data to calculate the distance. File .pickle can be calculate by src/codes/calcPickle.py by first build the postgis database based on osm data.
```bash
python calcPickle.py
```


## Usage
For now can be used by run codes in src/codes/

```python
pass
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
