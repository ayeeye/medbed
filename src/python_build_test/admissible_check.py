import pickle
from astar import haversine


with open('AllEdges.pickle', 'rb') as f:
    edgeList = pickle.load(f)
with open('AllSuccessors.pickle', 'rb') as f:
    adjList= pickle.load(f)
with open('AllNodes.pickle', 'rb') as f:
    nodeList = pickle.load(f)

number = 0
for node in nodeList:

      str_node = str(node.point)
      for successor in adjList[str_node]:
      	print(number)
      	print(haversine(node, successor))
      	print(successor.distance)
      	print()
      	number = number + 1
      	if haversine(node, successor) > successor.distance:
      		print("not admissible")
      		print("start:", node.point)
      		print("goal:", successor.point)
