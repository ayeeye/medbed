#!/usr/bin/env python

import math
import pickle
import psycopg2
import numpy as np
import pandas as pd
from point import Point
from pprint import pprint

conn = psycopg2.connect(dbname="gis", host="/var/run/postgresql/")
cur = conn.cursor()

def getEdges(size='large', dec_limit=15):
    if size=='large':
        cur.execute("""select st_y(st_transform(st_startpoint(way),4326)), st_x(st_transform(st_startpoint(way),4326)), 
                        st_y(st_transform(st_endpoint(way),4326)), st_x(st_transform(st_endpoint(way),4326)), st_length(way) 
                        from planet_osm_roads where highway='motorway' or highway='trunk' or highway='primary' or highway='secondary'
                        or highway='tertiary' or highway='unclassified' or highway='residential' or highway='motorway_link' or highway='trunk_link' 
                        or highway='primary_link' or highway='secondary_link' or highway='tertiary_link' or highway='living_street' or highway='service'
                        or highway='road' or highway='crossing' or highway='lane' or highway='track' or highway='shared_lane' or highway='motorway_junction'

                         """)
    elif size=='medium':
        cur.execute("""select st_y(st_transform(st_startpoint(way),4326)), st_x(st_transform(st_startpoint(way),4326)), 
                        st_y(st_transform(st_endpoint(way),4326)), st_x(st_transform(st_endpoint(way),4326)), st_length(way) 
                        from planet_osm_roads where highway='trunk' or highway='primary' or highway='motorway' or highway='secondary';""")
    else: 
        raise Exception("unknown size value")
    res = cur.fetchall()
    # points = set([src, dest])
    points = set()
    edgeList = []
    nodesList = []
    for edge in res:
        start = Point(edge[0], edge[1])
        dest = Point(edge[2], edge[3])

        if start not in points:
            points.add(start)
            nodesList.append(start)

        if dest not in points:
            points.add(dest)
            nodesList.append(dest)

        edgeList.append((start, dest, edge[4]))

    #adjframe = pd.DataFrame(index=points, columns=points)
    # for edge in edgeList:
    #     s = edge[0]
    #     d = edge[1]
    #     adjframe.at[s, d] = adjframe.at[d, s] = edge[2]
    # connect source and dest to nearby nodes
    # s = sorted([ (calcDist(node, src), node) for node in points ])
    # e = sorted([ (calcDist(node, dest), node) for node in points ])
    # pprint(s[:30])
    # pprint(e[:20])
    # exit()
    # for n in s[:25]:
    #     adjframe.at[src, n[1]] = adjframe.at[n[1], src] = n[0]
    #     edgeList.append((src, n[1], n[0]))
    # for n in e[:10]:
    #     adjframe.at[dest, n[1]] = adjframe.at[n[1], dest] = n[0]
    #     edgeList.append((n[1], dest, n[0]))
    return edgeList, points

def copyOf(copyNode, distance):
    cpoint = Point(copyNode.lat, copyNode.lon)
    cpoint.distance = distance
    return cpoint

def getSucc(node, edgeList):
    # print("masuk getsucc")
    succ = []
    for edge in edgeList:
        if edge[0] == node and edge[1] != node:
            succ.append(copyOf(edge[1], edge[2]))
    # string_node = str(node.point)
    return succ


global global_succ
global_succ = {}

def getAdjList(edgeList):
    global global_succ

    for edge in edgeList:

        node = edge[0]
        string_node = str(node.point)
        succ = []
        if global_succ.get(string_node):
            # print("masuk if")
            continue
        else: 
            # print("masuk else")
            succ = getSucc(node, edgeList)
            global_succ[string_node] = succ
        if(len(global_succ)%100 == 0):
            print("len global_succ", len(global_succ))

    for edge in edgeList:
        node = edge[1]
        string_node = str(node.point)
        succ = []
        if global_succ.get(string_node):
            # print("masuk if")
            continue
        else: 
            # print("masuk else")
            succ = getSucc(node, edgeList)
            global_succ[string_node] = succ
        # print("len global_succ", len(global_succ))

def calcMatrix(origins, destinations):
    distMatrix = [ [ 0 for i in range(len(destinations)) ] for j in range(len(origins)) ]
    for i in range(len(origins)):
        for j in range(len(destinations)):
            dist = calcDist(origins[i]["latitude"], origins[i]["longitude"], destinations[j]["latitude"], destinations[j]["longitude"])
            distMatrix[i][j] = dist/1000 # in Km
    return distMatrix

def main():
    #size = input('Enter required size of graph to calculate Distance matrix (medium, large, wards) (default: large): ') # 'medium'
    size = 'large'
    # if size != 'medium' and size != 'wards': size = 'large'
    print(f"Creating dataframe for size: {size} ...")
    # src = Point(-6.144823, 106.747216) # bits
    # dest = Point(-6.175147, 106.827142) # rgia
    # print(1)
    # adjframe, edgeList, nodes = getEdges(src, dest, size)
    edgeList, nodes = getEdges(size)
    getAdjList(edgeList)
    # print(2)
    # adjframe.to_pickle(size+'Adjframe.pkl')

    with open('SelectedEdges.pickle', 'wb') as f:
        edgeList = pickle.dump(edgeList, f)
    
    with open('SelectedNodes.pickle', 'wb') as f:
        pickle.dump(nodes, f)

    with open('SelectedSuccessors.pickle', 'wb') as f:
        pickle.dump(global_succ, f)

    
    
    # with open(size+'Edges.pkl', 'wb') as f:
    #     pickle.dump(edgeList, f)
    # print("Edge and Adjacency Matrices created!")
    

    # nodes = [Point(17.5472, 78.5725), Point(17.492374, 78.336394), Point(17.2408, 78.4293), Point(17.495428, 78.335287)]
    # nodes.append(Point(17.547323, 78.572519)) # bits
    # nodes.append(Point(17.240852, 78.429417)) # rgia
    # distMatrix = calcMatrix(origins, destinations)
    # print_matrix(distMatrix)

if __name__ == "__main__":
    main()
