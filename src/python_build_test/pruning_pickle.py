import pickle
import pandas as pd
import json
import gmplot
import webbrowser


with open('OptimizedAllEdges.pickle', 'rb') as f:
  edgeList = pickle.load(f)
with open('OptimizedAllSuccessors.pickle', 'rb') as f:
  adjList= pickle.load(f)
with open('OptimizedAllNodes.pickle', 'rb') as f:
  nodeList = pickle.load(f)


result_nodes = set()
number = len(nodeList)
idx = 0
while(number > 0):
  number = number - 1
  if(idx%100 == 0):
    print("idx:", idx)
    print("len result_nodes:", len(result_nodes))

  visited = set()

  start = nodeList[idx]

  idx = idx + 1

  queue = [start]

  visited.add(str(start.point))

  while len(queue) > 0:
    current = queue.pop(0)
    successors = adjList[str(current.point)]
    for successor in successors:
      if str(successor.point) not in visited:
        visited.add(str(successor.point))
        queue.append(successor)
  # print("--------------------------------------")
  # print("number_while", number_while)
  # print("number_for", number_for)
  # print("len visited",len(visited))
  # print("len nodeList", len(nodeList))
  # print("len edgeList", len(edgeList))
  # print("len adjList", len(adjList))

  if(len(visited) != 14157): continue
  else: result_nodes.add(str(start.point))


print("len result_nodes:", len(result_nodes))

new_edgeList = []
new_adjList = {}
new_nodeList = []

for edge in edgeList:
  if str(edge[0].point) in result_nodes and str(edge[1].point) in result_nodes:
    new_edgeList.append(edge)

for node in adjList:
  if node in result_nodes:
    new_successor = []
    for successor in adjList[node]:
      if str(successor.point) in result_nodes:
        new_successor.append(successor)
    new_adjList[node] = new_successor

for node in nodeList:
  if str(node.point) in result_nodes:
    new_nodeList.append(node)

print("len new_nodeList", len(new_nodeList))
print("len new_edgeList", len(new_edgeList))
print("len new_adjList", len(new_adjList))

# if count_dict.get(len(new_nodeList)):
#   count_dict[len(new_nodeList)] += 1
#   visited_dict[len(new_nodeList)].add(start)
# else:
#   count_dict[len(new_nodeList)] = 1

#   visited_dict[len(new_nodeList)] = set()
#   visited_dict[len(new_nodeList)].add(start)

# if(number%100 == 0):
#   print(number)
#   print(sorted(count_dict.items(), key=lambda x: x[1]))
  # print(visited_dict)



with open('TraversalAllEdges.pickle', 'wb') as f:
      edgeList = pickle.dump(new_edgeList, f)

with open('TraversalAllNodes.pickle', 'wb') as f:
    pickle.dump(new_nodeList, f)

with open('TraversalAllSuccessors.pickle', 'wb') as f:
    pickle.dump(new_adjList, f)

with open("APIkeys.json", 'r') as f:
    api_key = json.loads(f.read())['GoogleMapsKey']


with open("allEdges.csv", 'w') as f:
  index = 1
  f.write("index,latitude,longitude\n")
  for point in new_nodeList:
      f.write(f"{index},{point.lat},{point.lon}\n")
      index = index + 1

df = pd.read_csv("allEdges.csv").drop(columns=["index"])
gmap = gmplot.GoogleMapPlotter(-6.175147, 106.827142, 11.5, apikey=api_key) # -6.175147, 106.827142 dari mana ya, batas kasar jakarta, buat map view
gmap.scatter(df['latitude'], df['longitude'], '#00FF00', size = 10, marker = False)
gmap.plot(df['latitude'], df['longitude'], 'cornflowerblue', edge_width = 0.3)
gmap.draw("allEdges.html")
webbrowser.open("allEdges.html", new=2)








