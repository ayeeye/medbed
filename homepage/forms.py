from django import forms
from .point import Point

class InputForm(forms.Form):
        
	pilihan = [('VIP', 'VIP'), ('Kelas I', 'Kelas I'), ('Kelas II', 'Kelas II'), ('Kelas III', 'Kelas III'), ('ICU', 'ICU'), ('NICU', 'NICU'), ('PICU', 'PICU'), ('HCU', 'HCU'), ('ICCU', 'ICCU'), ('Isolasi', 'Isolasi')]
	tingkat_kamar = forms.ChoiceField(label='Tingkat Kamar:', choices=pilihan)
	koordinat_awal = forms.CharField(label='Masukkan koordinat awal(lat, lon)', max_length=100)

	def __init__(self, *args, **kwargs):
		super(InputForm, self).__init__(*args, **kwargs)
		self.fields['tingkat_kamar'].widget.attrs['class'] = 'form-control form-control-sm col-8'
		self.fields['koordinat_awal'].widget.attrs['class'] = 'form-control form-control-sm col-8'
		

	def clean(self):
		cleaned_data = super(InputForm, self).clean()
		tingkat_kamar = cleaned_data.get('tingkat_kamar')
		koordinat = cleaned_data.get('koordinat_awal')
		if not tingkat_kamar and not koordinat_awal:
			raise forms.ValidationError('Form Kosong')

class SortForm(forms.Form):
	
	pilihan = [('rekomendasi', 'Rekomendasi'), ('kamar', 'Sisa Kamar'), ('aktif', 'Kasus COVID'), ('jarak', 'Jarak')]
	sort_by = forms.ChoiceField(label='Urut berdasarkan', choices=pilihan, widget=forms.Select(attrs={'onchange': 'submit();'}))

	def __init__(self, *args, **kwargs):
	    super(SortForm, self).__init__(*args, **kwargs)
	    self.fields['sort_by'].widget.attrs['class'] = 'form-control form-control-sm'
    

