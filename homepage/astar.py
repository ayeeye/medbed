import gmplot
import heapq
import json
import math
import pandas as pd
import pickle
import requests
import timeit
import webbrowser
import random
import os
import numpy as np

from bs4 import BeautifulSoup
from math import radians, cos, sin, asin, sqrt 
from .point import Point
from pprint import pprint
from pandas import read_excel

global jarak_user_ke_rs



###################################################################################################################################################

###################################################################################################################################################


def get_tingkat_helper(modal, list_rs, soup):
    results = soup.find(id=modal)
    tbody = results.find('tbody', class_='tbody-dashboard-new-modal')
    trows = tbody.find_all('tr')

    for trow in trows:
        tdetails = trow.find_all('td')
        tuple_rs = (tdetails[2].string, int(tdetails[3].string) )
        list_rs.append(tuple_rs)

    return list_rs

def get_tingkat(tingkat):
    URL = "http://eis.dinkes.jakarta.go.id/dashboard.php"
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    list_rs = []

    if (tingkat == "vip"):
        list_rs = get_tingkat_helper('modal1', list_rs, soup)

    elif (tingkat == "kelas i"):
        list_rs = get_tingkat_helper('modal2', list_rs, soup)

    elif (tingkat == "kelas ii"):
        list_rs = get_tingkat_helper('modal3', list_rs, soup)

    elif (tingkat == "kelas iii"):
        list_rs = get_tingkat_helper('modal4', list_rs, soup)

    elif (tingkat == "icu"):
        list_rs = get_tingkat_helper('modal5', list_rs, soup)

    elif (tingkat == "nicu"):
        list_rs = get_tingkat_helper('modal6', list_rs, soup)

    elif (tingkat == "picu"):
        list_rs = get_tingkat_helper('modal7', list_rs, soup)

    elif (tingkat == "hcu"):
        list_rs = get_tingkat_helper('modal8', list_rs, soup)

    elif (tingkat == "iccu"):
        list_rs = get_tingkat_helper('modal9', list_rs, soup)

    elif (tingkat == "isolasi"):
        list_rs = get_tingkat_helper('modal10', list_rs, soup)

    return list_rs





def haversine(p1, p2):
    """
    Fungsi untuk menghitung jarak antara dua koordinat menggunakan
    haversine formula, mengembalikan jarak dalam satuan meter
    """
    lat1 = p1.lat
    lon1 = p1.lon
    lat2 = p2.lat
    lon2 = p2.lon
    radius = 6371e3; # kmetres
    phi1 = math.radians(lat1)
    phi2 = math.radians(lat2)
    delta_phi = math.radians(lat2-lat1)
    delta_lam = math.radians(lon2-lon1)
    haversine_form = math.sin(delta_phi/2)*math.sin(delta_phi/2)+math.cos(phi1)*math.cos(phi2)* \
                    math.sin(delta_lam/2)*math.sin(delta_lam/2)
    distance = 2 * radius * math.atan2(math.sqrt(haversine_form), math.sqrt(1-haversine_form))
    return distance



def getPath(start, stop):
    path = [ stop ]
    while path[-1] != start:
        path.append(path[-1].parent)
    path.reverse()
    return path

def showPathOnMap(filename, filename_template, start, stop, waypoints_lst):
    with open("src/json/api_keys.json", 'r') as f:
        api_key = json.loads(f.read())['GoogleMapsKey']

    df = pd.read_csv(filename+".csv").drop(columns=["index"])

    center_lat = (start.lat + stop.lat) / 2
    center_lon = (start.lon + stop.lon) / 2

    gmap = gmplot.GoogleMapPlotter(center_lat, center_lon, 12, apikey=api_key)
    gmap.scatter(df['latitude'], df['longitude'], '#00FF00', size = 50, marker = False)
    gmap.plot(df['latitude'], df['longitude'], 'cornflowerblue', edge_width = 3.0)
    # gmap.directions(
    # (-6.343959745430423, 106.82543184622318),
    # (-6.354580120889912, 106.85796167747661))
    gmap.marker(start.lat, start.lon, label='Start', color='green', title='Start')
    gmap.marker(stop.lat, stop.lon, label='Goal', color='red', title='Goal')
    gmap.draw(filename_template+".html")



def astar(rs, start, stop, adj_list): 
    """
    comment
    """

    global jarak_user_ke_rs
    start.g = 0
    start.h = haversine(start, stop)
    start.f = start.g + start.h
    start.distance = 0
    stop.h = 0

    """
    Membuat priority queue untuk expand node
    """
    opened = [ start ]
    
    """
    Membuat daftar node yang sudah pernah diexplore
    """
    closed = set()

    done = False

    while len(opened) > 0 and not done:
        # print("masuk while")
        
        """
        Mengambil node successors dari current node yang sedang diexplore
        """
        current = heapq.heappop(opened) 
        current_str= str(current.point)
        successors = adj_list[current_str]

        """
        Menambahkan current node yang sedang diexplore ke dalam set node yang sudah pernah diexplore
        """
        closed.add(current)


        for successor in successors:
            # print("masuk for")
            # print("*****************for astar*******************")
            if successor == stop:
                successor.parent = current
                successor.parent = current
                done = True
                break
            if successor in closed:
                continue
            else:

                successor.parent = current
                successor.g = current.g + successor.distance
                successor.h = haversine(successor, stop)
                successor.f = successor.g + successor.h

            heapq.heappush(opened, successor)



    tuple_point = sorted([ (haversine(point, stop), point) for point in closed ])

    total_distance = 0
    end_point = tuple_point[0][1]
    curr_point = end_point

    while(curr_point.parent != None and curr_point.parent != start):
        total_distance = total_distance + curr_point.distance
        curr_point = curr_point.parent

    total_distance = total_distance + curr_point.distance
    jarak_user_ke_rs[rs] = total_distance    

    # jarak_user_ke_rs[rs] = end_point.g

    
    return getPath(start, end_point)





def closer(lat_user, lon_user, nodes):
    """
    Find nearest documented edge from input
    """
    distance = float('inf')
    result = None

    lat = lat_user
    lon = lon_user
    initial = Point(lat, lon)
    for node in nodes:
        curr_distance = haversine(initial, node)
        if curr_distance < distance:
            distance = curr_distance
            result = node

    return result






def normalize(list_data):
    """
    Method untuk menormalisasi data sehingga jarak dan jumlah kasus dapat diproses dalam skala yang sama
    Fitur ini menggunakan modified min-max normalization untuk mencegah outlier maksimum mengganggu skala
    """
    list_result = []
    mean = np.mean(list_data)
    std = np.std(list_data)

    min_data = min(list_data)
    max_data = int(mean + 2*std)

    for data in list_data:
        norm_data = (data - min_data)/(max_data - min_data)
        if norm_data >= 1:
            norm_data = 100
        list_result.append(norm_data)

    return list_result


def get_rekomendasi(list_data1, list_data2):
    """
    Ngereturn list nilai rekomendasi.
    Konsider proporsinya
    """
    list_result = []
    processed_list1 = normalize(list_data1)
    processed_list2 = normalize(list_data2)

    for i in range(len(processed_list1)):
        final = 1- ((processed_list1[i] + processed_list2[i])/2)
        if final <= 0:
            list_result.append(str(0))
        else:
            list_result.append("{:.2f}".format(final*100))
    
    return list_result



def get_kasus_aktif(output_rs):
    """
    Pakai data kecamatan
    """

    #REKOMENDASI RS, JUMLAHKAN JARAK DAN KASUS COVID, DIDAPAT JARAK DAN KASUS COVID YANG PALING KECIL

    sheet_id = '1rDHNOFsHbro0OZ971hVpmXBWD_i5XwVp'
    sheet_name = '1023893780'
    df = pd.read_csv(f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv&gid={sheet_name}")
    

    Tarakan = Cempaka_Putih = Johar_Baru = Sawah_Besar = Kemayoran = Tanah_Abang = 0
    Koja = Cilincing = Pademangan = Tugu_Koja = Tanjung_Priok = 0
    Pasar_Minggu = Kebayoran_Baru = Jati_Padang = Kebayoran_Lama = Tebet = Mampang_Prapatan = Jagakarsa = Pesanggrahan = 0
    Pasar_Rebo = Budhi_Asih = Duren_Sawit = Adhyaksa = Ciracas = Kramat_Jati = Matraman = Cipayung = 0
    Cengkareng = Kembangan = Kalideres = Taman_Sari = Kepulauan_Seribu = Ibu_dan_Anak_Budhi_Jaya = 0


    for i in range(1,len(df.nama_kecamatan)):
        if(df.nama_kecamatan[i] == "GAMBIR"):
            Tarakan += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "CEMPAKA PUTIH"):
            Cempaka_Putih += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "JOHAR BARU"):
            Johar_Baru += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "SAWAH BESAR"):
            Sawah_Besar += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KEMAYORAN"):
            Kemayoran += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "TANAH ABANG"):
            Tanah_Abang += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KOJA"):
            Koja += int(df['Dirawat'][i] + df['Self Isolation'][i])
            Tugu_Koja += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "CILINCING"):
            Cilincing += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "PADEMANGAN"):
            Pademangan += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "TANJUNG PRIOK"):
            Tanjung_Priok += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "PASAR MINGGU"):
            Jati_Padang += int(df['Dirawat'][i] + df['Self Isolation'][i])
            Pasar_Minggu += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KEBAYORAN BARU"):
            Kebayoran_Baru += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KEBAYORAN LAMA"):
            Kebayoran_Lama += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "TEBET"):
            Tebet += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "MAMPANG PRAPATAN"):
            Mampang_Prapatan += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "JAGAKARSA"):
            Jagakarsa+= int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "PESANGGRAHAN"):
            Pesanggrahan += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "PASAR REBO"):
            Pasar_Rebo += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KRAMAT JATI"):
            Budhi_Asih += int(df['Dirawat'][i] + df['Self Isolation'][i])
            Kramat_Jati += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "DUREN SAWIT"):
            Duren_Sawit += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "CIPAYUNG"):
            Adhyaksa += int(df['Dirawat'][i] + df['Self Isolation'][i])
            Cipayung += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "CIRACAS"):
            Ciracas += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "MATRAMAN"):
            Matraman += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "CENGKARENG"):
            Cengkareng += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KEMBANGAN"):
            Kembangan += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KALI DERES"):
            Kalideres += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "TAMAN SARI"):
            Taman_Sari += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "KEP. SERIBU UTARA"):
            Kepulauan_Seribu += int(df['Dirawat'][i] + df['Self Isolation'][i])
        elif(df.nama_kecamatan[i] == "SETIA BUDI"):
            Ibu_dan_Anak_Budhi_Jaya += int(df['Dirawat'][i] + df['Self Isolation'][i])


    output_covid = output_rs.copy()
    kasus_aktif = []
    for i in range(len(output_covid)):
        if  "Tarakan" in output_covid[i][0]:
            output_covid[i][5] += Tarakan
            kasus_aktif.append(Tarakan)
        elif "Cempaka" in output_covid[i][0]:
            output_covid[i][5] += Cempaka_Putih
            kasus_aktif.append(Cempaka_Putih)
        elif "Johar" in output_covid[i][0]:
            output_covid[i][5] += Johar_Baru
            kasus_aktif.append(Johar_Baru)
        elif "Sawah" in output_covid[i][0]:
            output_covid[i][5] += Sawah_Besar
            kasus_aktif.append(Sawah_Besar)
        elif "Kemayoran" in output_covid[i][0]:
            output_covid[i][5] += Kemayoran
            kasus_aktif.append(Kemayoran)
        elif "Tanah" in output_covid[i][0]:
            output_covid[i][5] += Tanah_Abang
            kasus_aktif.append(Tanah_Abang)
        elif "Koja" in output_covid[i][0]:
            output_covid[i][5] += Koja
            kasus_aktif.append(Koja)
        elif "Cilincing" in output_covid[i][0]:
            output_covid[i][5] += Cilincing
            kasus_aktif.append(Cilincing)
        elif "Pademangan" in output_covid[i][0]:
            output_covid[i][5] += Pademangan
            kasus_aktif.append(Pademangan)
        elif "Tugu" in output_covid[i][0]:
            output_covid[i][5] += Tugu_Koja
            kasus_aktif.append(Tugu_Koja)
        elif "Tanjung" in output_covid[i][0]:
            output_covid[i][5] += Tanjung_Priok
            kasus_aktif.append(Tanjung_Priok)
        elif "Minggu" in output_covid[i][0]:
            output_covid[i][5] += Pasar_Minggu
            kasus_aktif.append(Pasar_Minggu)
        elif "Baru" in output_covid[i][0]:
            output_covid[i][5] += Kebayoran_Baru
            kasus_aktif.append(Kebayoran_Baru)
        elif "Padang" in output_covid[i][0]:
            output_covid[i][5] += Jati_Padang
            kasus_aktif.append(Jati_Padang)
        elif "Lama" in output_covid[i][0]:
            output_covid[i][5] += Kebayoran_Lama
            kasus_aktif.append(Kebayoran_Lama)
        elif "Tebet" in output_covid[i][0]:
            output_covid[i][5] += Tebet
            kasus_aktif.append(Tebet)
        elif "Mampang" in output_covid[i][0]:
            output_covid[i][5] += Mampang_Prapatan
            kasus_aktif.append(Mampang_Prapatan)
        elif "Jagakarsa" in output_covid[i][0]:
            output_covid[i][5] += Jagakarsa
            kasus_aktif.append(Jagakarsa)
        elif "Pesanggrahan" in output_covid[i][0]:
            output_covid[i][5] += Pesanggrahan
            kasus_aktif.append(Pesanggrahan)
        elif "Rebo" in output_covid[i][0]:
            output_covid[i][5] += Pasar_Rebo
            kasus_aktif.append(Pasar_Rebo)
        elif "Asih" in output_covid[i][0]:
            output_covid[i][5] += Budhi_Asih
            kasus_aktif.append(Budhi_Asih)
        elif "Sawit" in output_covid[i][0]:
            output_covid[i][5] += Duren_Sawit
            kasus_aktif.append(Duren_Sawit)
        elif "Adhyaksa" in output_covid[i][0]:
            output_covid[i][5] += Adhyaksa
            kasus_aktif.append(Adhyaksa)
        elif "Ciracas" in output_covid[i][0]:
            output_covid[i][5] += Ciracas
            kasus_aktif.append(Ciracas)
        elif "Kramat" in output_covid[i][0]:
            output_covid[i][5] += Kramat_Jati
            kasus_aktif.append(Kramat_Jati)
        elif "Matraman" in output_covid[i][0]:
            output_covid[i][5] += Matraman
            kasus_aktif.append(Matraman)
        elif "Cipayung" in output_covid[i][0]:
            output_covid[i][5] += Cipayung
            kasus_aktif.append(Cipayung)
        elif "Cengkareng" in output_covid[i][0]:
            output_covid[i][5] += Cengkareng
            kasus_aktif.append(Cengkareng)
        elif "Kembangan" in output_covid[i][0]:
            output_covid[i][5] += Kembangan
            kasus_aktif.append(Kembangan)
        elif "Kalideres" in output_covid[i][0]:
            output_covid[i][5] += Kalideres
            kasus_aktif.append(Kalideres)
        elif "Taman" in output_covid[i][0]:
            output_covid[i][5] += Taman_Sari
            kasus_aktif.append(Taman_Sari)
        elif "Seribu" in output_covid[i][0]:
            output_covid[i][5] += Kepulauan_Seribu
            kasus_aktif.append(Kepulauan_Seribu)
        elif "Ibu" in output_covid[i][0]:
            output_covid[i][5] += Ibu_dan_Anak_Budhi_Jaya
            kasus_aktif.append(Ibu_dan_Anak_Budhi_Jaya)
        
    output = [output_covid, kasus_aktif]
    return output




###################################################################################################################################################

###################################################################################################################################################





def main(tingkat, entry):
    global jarak_user_ke_rs

    nama_kelas = ['vip', 'kelas i', 'kelas ii', 'kelas iii', 'icu', 'nicu', 'picu', 'hcu', 'iccu', 'isolasi']

    arr_entry = entry.strip().split(',')
    lat_user = float(arr_entry[0])
    lon_user = float(arr_entry[1])




    
    """
    Comment data yang dibuka apaan
    """

    import sys
    sys.path.append(r'homepage')
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    with open(BASE_DIR+'/src/pickle/optimized_new_edges.pickle', 'rb') as f:
        edge_list = pickle.load(f)
    with open(BASE_DIR+'/src/pickle/optimized_new_successors.pickle', 'rb') as f:
        adj_list= pickle.load(f)
    with open(BASE_DIR+'/src/pickle/optimized_new_nodes.pickle', 'rb') as f:
        node_list = pickle.load(f)
    with open(BASE_DIR+'/src/pickle/rs_latlon.pickle', 'rb') as f:
        list_latlong_rs = pickle.load(f)
    with open(BASE_DIR+'/src/pickle/rs_jarak.pickle', 'rb') as f:
        jarak_user_ke_rs = pickle.load(f)



    list_rs = get_tingkat(tingkat.lower())


    start = closer(lat_user, lon_user, node_list)

    for rs in list_rs:

        latlong_rs = list_latlong_rs.get(rs[0])
        point_rs = closer(latlong_rs[0], latlong_rs[1], node_list)

        """
        Menghasilkan path yang berisi point dari pemanggilan fungsi astar
        """
        path = astar(rs[0], start, point_rs, adj_list)

        """
        Menulis di FinalPath.csb untuk diproses fungsi showPathOnMap
        """    

        filename = BASE_DIR+"/src/route/"+"_".join(rs[0].lower().split())
        filename_template = BASE_DIR+"/templates/route/"+"_".join(rs[0].lower().split())

        with open(filename_template+".html", 'w') as f:
            f.write("<!-- DummyText -->")
            f.close()

        waypoints_lst = []
        with open(filename+".csv", 'w') as f:
            index = 1
            f.write("index,latitude,longitude\n")
            for point in path:
                f.write(f"{index},{point.lat},{point.lon}\n")
                waypoints_lst.append((point.lat, point.lon))
                index += 1
            f.write(f"{index},{point_rs.lat}, {point_rs.lon}\n")
            f.close()
        showPathOnMap(filename, filename_template, start, point_rs, waypoints_lst)

    sort_jarak_user_ke_rs = sorted(jarak_user_ke_rs.items(), key=lambda x: x[1])

    output_rs = []

    idx_list_rs = 0

    # Craft List of Distance and Covid-19 Cases to get Recommendation Aggregation
    list_jarak = []
    list_rekomendasi = []

    for jarak in sort_jarak_user_ke_rs:
        # If value satisfies the condition, then store it in new_dict
        if jarak[1] != float("inf"):
            nama_rs_url = "+".join(jarak[0].split())
            url_gmaps = "https://www.google.com/maps/dir/?api=1&origin=" + \
                        str(start.lat)+","+str(start.lon) + "&destination=" + \
                        nama_rs_url + "&travelmode=driving"
            kasus_aktif = 0
            rekomendasi = 0
            nama_rs = jarak[0]
            jarak_rs = int(jarak[1]/100)/10

            list_jarak.append(jarak_rs)

            sisa_kamar = list_rs[idx_list_rs][1]
            nama_rs_url = "-".join(nama_rs.lower().split())
            route_rs_url = "_".join(nama_rs.lower().split())

            output_rs.append( [nama_rs, jarak_rs, url_gmaps, nama_rs_url, sisa_kamar, kasus_aktif, rekomendasi, route_rs_url ])
            idx_list_rs = idx_list_rs + 1

    
    annotated_output_rs = get_kasus_aktif(output_rs)
    output_rs = annotated_output_rs[0]
    kasus_aktif = annotated_output_rs[1]
    
    list_rekomendasi = get_rekomendasi(list_jarak, kasus_aktif)
    for i in range(len(list_rekomendasi)):
        output_rs[i][6] = list_rekomendasi[i]

    return output_rs

if __name__ == "__main__":
    main()

