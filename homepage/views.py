from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import InputForm, SortForm
from .astar import main as astar
from .point import Point
from .webscrape import *
from django.contrib.staticfiles.storage import staticfiles_storage



def index(request):
	return redirect("/home/")


def home(request):

	form = InputForm()
	context_input = {'form': form}
	
	if request.method == 'POST':
		form = InputForm(request.POST)
		if form.is_valid():
			tingkat_kamar = form.cleaned_data['tingkat_kamar']
			koordinat_awal = form.cleaned_data['koordinat_awal']
			rs_result = astar(tingkat_kamar, koordinat_awal)
			request.session['rs_result'] = rs_result

			return redirect("/rekomendasi/")

	return render(request, 'homepage/index.html', context_input)


def rekomendasi(request):
	if 'sort_by' not in request.session:
		request.session['sort_by'] = 'rekomendasi'
	
	if request.method == 'POST':
		form = SortForm(request.POST)
		if form.is_valid():
			sort_by = form.cleaned_data['sort_by']
			request.session['sort_by'] = sort_by
			return redirect("/rekomendasi/")

	rs_result = request.session['rs_result']

	sort_by = request.session['sort_by']
	comment_sort = "default"

	if sort_by == 'kamar':
		rs_result = sorted(rs_result, key=lambda l:l[4], reverse=True)
		comment_sort = "jumlah sisa kamar yang terbanyak"
	if sort_by == 'aktif':
		rs_result = sorted(rs_result, key=lambda l:l[5])
		comment_sort = "jumlah kasus aktif terkecil"
	if sort_by == 'rekomendasi':
		rs_result = sorted(rs_result, key=lambda l:l[6], reverse=True)
		comment_sort = "algoritma rekomendasi MedBed"
	if sort_by == 'jarak':
		rs_result = sorted(rs_result, key=lambda l:l[1])
		comment_sort = "jarak terdekat"


	form = SortForm(initial={'sort_by': sort_by})
	context_sort = {'form': form, 'rs_result': rs_result, 'comment_sort': comment_sort}

	return render(request, 'homepage/rekomendasi.html', context_sort)


def detail(request, image_url):
        rs = get_rs(image_url)
        rs[0] = rs[0].replace("_", "-")
        return render(request, 'homepage/detail.html', {'rs': rs})


def about(request):
	return render(request, 'homepage/about.html')

def route(request, image_url):
	url = 'route/'+image_url+'.html'
	return render(request, url)
