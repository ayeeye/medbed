from bs4 import BeautifulSoup
import requests
import pandas as pd

URL = "http://eis.dinkes.jakarta.go.id/dashboard.php"
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')

global dict_rs
global webscraped
global df

sheet_id = '1rDHNOFsHbro0OZ971hVpmXBWD_i5XwVp'
sheet_name = '1023893780'
df = pd.read_csv(f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv&gid={sheet_name}")

def get_kasus_aktif(rs):
    """
    Pakai data kecamatan
    """
    for i in range(1,len(df.nama_kecamatan)):
        if(df.nama_kecamatan[i] == rs):
            return int(df['Dirawat'][i] + df['Self Isolation'][i])
    
    return 0

dict_rs = {
  "rsud_tarakan": ["rsud_tarakan", "RSUD Tarakan", "Jl. Kyai Caringin No.7, RT.11/RW.4, Cideng, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10150, Indonesia", "0213503003", "tel:0213503003", get_kasus_aktif("GAMBIR")],
  "rsud_cempaka_putih": ["rsud_cempaka_putih", "RSUD Cempaka Putih", "Jl. Rawasari Sel. No.1, RT.16/RW.2, Cemp. Putih Tim., Kec. Cemp. Putih, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10510, Indonesia", "0213503003", "tel:0213503003", get_kasus_aktif("CEMPAKA PUTIH")],
  "rsud_johar_baru": ["rsud_johar_baru", "RSUD Johar Baru", "Jl. Tanah Tinggi XII No.15-23, RT.10/RW.9, Tanah Tinggi, Kec. Johar Baru, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10540, Indonesia", "02121470344", "tel:02121470344", get_kasus_aktif("JOHAR BARU")],
  "rsud_sawah_besar": ["rsud_sawah_besar", "RSUD Sawah Besar", "Jl. Dwiwarna Raya No.6-8, RW.9, Karang Anyar, Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10740, Indonesia", "02162320819", "tel:02162320819", get_kasus_aktif("SAWAH BESAR")],
  "rsud_kemayoran": ["rsud_kemayoran", "RSUD Kemayoran", "Jl. Serdang I No.3, RT.6/RW.5, Serdang, Kec. Kemayoran, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10650, Indonesia", "0214244277", "tel:0214244277", get_kasus_aktif("KEMAYORAN")],
  "rsud_tanah_abang": ["rsud_tanah_abang", "RSUD Tanah Abang", "Jl. KH. Mas Mansyur No. 30, RT. 05 / RT. 07, RT.5/RW.7, Kb. Kacang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10240, Indonesia", "0213150427", "tel:0213150427", get_kasus_aktif("TANAH ABANG")],
  "rsud_koja": ["rsud_koja", "RSUD Koja", "Jl. Deli No.4, RT.11/RW.7, Koja, Kec. Koja, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14220, Indonesia", "02143938478", "tel:02143938478", get_kasus_aktif("KOJA")],
  "rsud_cilincing": ["rsud_cilincing", "RSUD Cilincing", "Jl. Madya Kebantenan No.4, RT.4/RW.2, Semper Tim., Kec. Cilincing, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14130, Indonesia", "0214416367", "tel:0214416367", get_kasus_aktif("CILINCING")],
  "rsud_pademangan": ["rsud_pademangan", "RSUD Pademangan", "Jalan Budi Mulia Raya No. 2 RT. 15 / RW. 11 Pademangan Barat Pademangan RT.11/RW.11, Kec. Pademangan Kota Jakarta Utara, Daerah Khusus Ibukota, RT.11/RW.11, Pademangan Bar., Kec. Pademangan, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14420, Indonesia", "0216452346", "tel:0216452346", get_kasus_aktif("PADEMANGAN")],
  "rsud_tugu_koja": ["rsud_tugu_koja", "RSUD Tugu Koja", "Jl. Walang Permai No.39, RT.6/RW.12, Tugu Utara, Kec. Koja, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14260, Indonesia", "02126061110", "tel:02126061110", get_kasus_aktif("KOJA")],
  "rsud_tanjung_priok": ["rsud_tanjung_priok", "RSUD Tanjung Priok", "Jalan Bugis No.3, Kebon Bawang, Tanjung Priok, RT.1/RW.4, Kb. Bawang, Tj. Priok, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14320, Indonesia", "02143908923", "tel:02143908923", get_kasus_aktif("TANJUNG PRIOK")],
  "rsud_pasar_minggu": ["rsud_pasar_minggu", "RSUD Pasar Minggu", "Jl. TB Simatupang No.1, RT.1/RW.5, Ragunan, Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12550, Indonesia", "02129059999", "tel:02129059999", get_kasus_aktif("PASAR MINGGU")],
  "rsud_kebayoran_baru": ["rsud_kebayoran_baru", "RSUD Kebayoran Baru", "Jl. Abdul Majid Raya No.28, RT.2/RW.5, Cipete Utara, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12150, Indonesia", "02122774429", "tel:02122774429", get_kasus_aktif("KEBAYORAN BARU")],
  "rsud_jati_padang": ["rsud_jati_padang", "RSUD Jati Padang", "Jl. Raya Ragunan No.16, RT.11/RW.11, Ps. Minggu, Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12740, Indonesia", "02122784477", "tel:02122784477", get_kasus_aktif("PASAR MINGGU")],
  "rsud_kebayoran_lama": ["rsud_kebayoran_lama", "RSUD Kebayoran Lama", "Jl. Jatayu No.RT.1, RT.1/RW.12, Kby. Lama Sel., Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12240, Indonesia", "02127939067", "tel:02127939067", get_kasus_aktif("KEBAYORAN LAMA")],
  "rsud_tebet": ["rsud_tebet", "RSUD Tebet", "Jl. Prof. DR. Soepomo SH No.54, RT.13/RW.2, Tebet Bar., Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12810, Indonesia", "0218314955", "tel:0218314955", get_kasus_aktif("TEBET")],
  "rsud_mampang_prapatan": ["rsud_mampang_prapatan", "RSUD Mampang Prapatan", "Jl. Kapten Tendean No.9, RT.1/RW.5, Mampang Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12710, Indonesia", "0217971115", "tel:0217971115", get_kasus_aktif("MAMPANG PRAPATAN")],
  "rsud_jagakarsa": ["rsud_jagakarsa", "RSUD Jagakarsa", "Jl. Moh. Kahfi 1 No.27 A, RT.1/RW.6, Jagakarsa, Kec. Jagakarsa, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12630, Indonesia", "02178882455", "tel:02178882455", get_kasus_aktif("JAGAKARSA")],
  "rsud_pesanggrahan": ["rsud_pesanggrahan", "RSUD Pesanggrahan", "Jl. Cenek I No.1, RT.5/RW.3, Pesanggrahan, Kec. Pesanggrahan, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12320, Indonesia", "0217356087", "tel:0217356087", get_kasus_aktif("PESANGGRAHAN")],
  "rsud_pasar_rebo": ["rsud_pasar_rebo", "RSUD Pasar Rebo", "Jl. TB Simatupang No.30, RT.9/RW.2, Gedong, Kec. Ps. Rebo, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13760, Indonesia", "0218401127", "tel:0218401127", get_kasus_aktif("PASAR REBO")],
  "rsud_budhi_asih": ["rsud_budhi_asih", "RSUD Budhi Asih", "Jl. Dewi Sartika No.200, RT.1/RW.4, Cawang, Kec. Kramat jati, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13630, Indonesia", "0218090282", "tel:0218090282", get_kasus_aktif("KRAMAT JATI")],
  "rskd_duren_sawit": ["rskd_duren_sawit", "RSKD Duren Sawit", "Jl. Duren Sawit Baru No.2, RT.16/RW.6, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430, Indonesia", "0218628686", "tel:0218628686", get_kasus_aktif("DUREN SAWIT")],
  "rsu_adhyaksa": ["rsu_adhyaksa", "RSU Adhyaksa", "Jl. Duren Sawit Baru No.2, RT.16/RW.6, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430, Indonesia", "0218628686", "tel:0218628686", get_kasus_aktif("CIPAYUNG")],
  "rsud_ciracas": ["rsud_ciracas", "RSUD Ciracas", "Jl. Cibubur I No.RT.3, RT.13/RW.1, Cibubur, Kec. Ciracas, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13720, Indonesia", "02187711249", "tel:02187711249", get_kasus_aktif("CIRACAS")],
  "rsud_kramat_jati": ["rsud_kramat_jati", "RSUD Kramat Jati", "Jl. Raya Inpres No.48, RT.9/RW.9, Kp. Tengah, Kec. Kramat jati, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13540, Indonesia", "02187791352", "tel:02187791352", get_kasus_aktif("KRAMAT JATI")],
  "rsud_matraman": ["rsud_matraman", "RSUD Matraman", "Jalan Kebon Kelapa Raya No.29, Utan Kayu Utara, Matraman, RT.1/RW.10, Utan Kayu Sel., Kec. Matraman, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13120, Indonesia", "02185909055", "tel:02185909055", get_kasus_aktif("MATRAMAN")],
  "rsud_cipayung": ["rsud_cipayung", "RSUD Cipayung", "Jl. Mini III No.4, RT.4/RW.3, Bambu Apus, Kec. Cipayung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13890, Indonesia", "02185506588", "tel:02185506588", get_kasus_aktif("CIPAYUNG")],
  "rsud_cengkareng": ["rsud_cengkareng", "RSUD Cengkareng", "Jl. Kamal Raya Bumi Cengkareng Indah, Cengkareng Tim., Kecamatan Cengkareng, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11730, Indonesia", "02154372874", "tel:02154372874", get_kasus_aktif("CENGKARENG")],
  "rsud_kembangan": ["rsud_kembangan", "RSUD Kembangan", "Jl. Topas Raya Blok FII No.03, RT.15/RW.7, Meruya Utara, Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11620, Indonesia", "0215870834", "tel:0215870834", get_kasus_aktif("KEMBANGAN")],
  "rsud_kalideres": ["rsud_kalideres", "RSUD Kalideres", "Jl. Satu Maret No.48, RT.1/RW.4, Pegadungan, Kec. Kalideres, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11830, Indonesia", "02122552766", "tel:02122552766", get_kasus_aktif("KALI DERES")],
  "rsud_taman_sari": ["rsud_taman_sari", "RSUD Taman Sari", "Jl. Madu No.10, RT.4/RW.3, Mangga Besar, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11180, Indonesia", "02126564276", "tel:02126564276", get_kasus_aktif("TAMAN SARI")],
  "rsud_kepulauan_seribu": ["rsud_kepulauan_seribu", "RSUD Kepulauan Seribu", "Pulau Pramuka, Kelurahan Pulau Panggang, Kabupaten Administrasi Kepulauan Seribu, Daerah Khusus Ibukota Jakarta 14530, Indonesia", "02126071997", "tel:02126071997", get_kasus_aktif("KEP. SERIBU UTARA")],
  "rs_ibu_dan_anak_budhi_jaya": ["rs_ibu_dan_anak_budhi_jaya", "RS Ibu dan Anak Budhi Jaya", "Jalan Dokter Sahardjo No.120, RT.1/RW.8, Menteng Atas, Jakarta, RT.1/RW.8, Menteng Atas, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12960, Indonesia", "0218311722", "tel:0218311722", get_kasus_aktif("SETIA BUDI")]
}


webscraped = {
    "rsud_tarakan": False,
    "rsud_cempaka_putih": False,
    "rsud_johar_baru": False,
    "rsud_sawah_besar": False,
    "rsud_kemayoran": False,
    "rsud_tanah_abang": False,
    "rsud_koja": False,
    "rsud_cilincing": False,
    "rsud_pademangan": False,
    "rsud_tugu_koja": False,
    "rsud_tanjung_priok": False,
    "rsud_pasar_minggu": False,
    "rsud_kebayoran_baru": False,
    "rsud_jati_padang": False,
    "rsud_kebayoran_lama": False,
    "rsud_tebet": False,
    "rsud_mampang_prapatan": False,
    "rsud_jagakarsa": False,
    "rsud_pesanggrahan": False,
    "rsud_pasar_rebo": False,
    "rsud_budhi_asih": False,
    "rskd_duren_sawit": False,
    "rsu_adhyaksa": False,
    "rsud_ciracas": False,
    "rsud_kramat_jati": False,
    "rsud_matraman": False,
    "rsud_cipayung": False,
    "rsud_cengkareng": False,
    "rsud_kembangan": False,
    "rsud_kalideres": False,
    "rsud_taman_sari": False,
    "rsud_kepulauan_seribu": False,
    "rs_ibu_dan_anak_budhi_jaya": False    
}




'''
results = soup.find("tbody", {"class": "tbody-dashboard-new"})
trows = results.find_all('tr')

list_rs = []


for trow in trows:
    tdetails = trow.find_all('td')

    if (tdetails[1].string == "\r\n\t\t\t\t                    \tJakarta Pusat\t\t\t                    \t" or tdetails[1].string == "\r\n\t\t\t\t                    \tJakarta Utara\t\t\t                    \t" or tdetails[1].string == "\r\n\t\t\t\t                    \tJakarta Selatan\t\t\t                    \t" or tdetails[1].string == "\r\n\t\t\t\t                    \tJakarta Timur\t\t\t                    \t" or tdetails[1].string == "\r\n\t\t\t\t                    \tJakarta Barat\t\t\t                    \t" or tdetails[1].string == "\r\n\t\t\t\t                    \tKepulauan Seribu\t\t\t                    \t"):
	sisa_kamar = [tdetails[3].string, tdetails[4].string, tdetails[5].string, tdetails[6].string, tdetails[7].string, tdetails[8].string, tdetails[9].string, tdetails[10].string, tdetails[11].string]
	list_rs.append(

    else:
	sisa_kamar = [tdetails[2].string, tdetails[3].string, tdetails[4].string, tdetails[5].string, tdetails[6].string, tdetails[7].string, tdetails[8].string, tdetails[9].string, tdetails[10].string]

'''


def get_tingkat_helper(modal, list_rs, soup):
    results = soup.find(id=modal)
    tbody = results.find('tbody', class_='tbody-dashboard-new-modal')
    trows = tbody.find_all('tr')

    for trow in trows:
        tdetails = trow.find_all('td')
        list_rs_tingkat = [tdetails[2].string, int(tdetails[3].string)]
        list_rs.append(list_rs_tingkat)

    return list_rs

def get_tingkat(tingkat):
    URL = "http://eis.dinkes.jakarta.go.id/dashboard.php"
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    list_rs = []

    if (tingkat == "vip"):
        list_rs = get_tingkat_helper('modal1', list_rs, soup)

    elif (tingkat == "kelas i"):
        list_rs = get_tingkat_helper('modal2', list_rs, soup)

    elif (tingkat == "kelas ii"):
        list_rs = get_tingkat_helper('modal3', list_rs, soup)

    elif (tingkat == "kelas iii"):
        list_rs = get_tingkat_helper('modal4', list_rs, soup)

    elif (tingkat == "icu"):
        list_rs = get_tingkat_helper('modal5', list_rs, soup)

    elif (tingkat == "nicu"):
        list_rs = get_tingkat_helper('modal6', list_rs, soup)

    elif (tingkat == "picu"):
        list_rs = get_tingkat_helper('modal7', list_rs, soup)

    elif (tingkat == "hcu"):
        list_rs = get_tingkat_helper('modal8', list_rs, soup)

    elif (tingkat == "iccu"):
        list_rs = get_tingkat_helper('modal9', list_rs, soup)

    elif (tingkat == "isolasi"):
        list_rs = get_tingkat_helper('modal10', list_rs, soup)

    return list_rs

def get_rs(name):

    if (webscraped.get(name) == False):
        print("This is false")
        get_rs_helper(name, "vip")
        get_rs_helper(name, "kelas i")
        get_rs_helper(name, "kelas ii")
        get_rs_helper(name, "kelas iii")
        get_rs_helper(name, "icu")
        get_rs_helper(name, "nicu")
        get_rs_helper(name, "picu")
        get_rs_helper(name, "hcu")
        get_rs_helper(name, "iccu")
        get_rs_helper(name, "isolasi")

        webscraped[name] = True
        
    return dict_rs.get(name)


def get_rs_helper(name, tingkat):
    present = False

    rs = dict_rs.get(name)[1]

    list_rs_tingkat = get_tingkat(tingkat)
    for item in list_rs_tingkat:
        if item[0] == rs:
            dict_rs.get(name).append(item[1])
            present = True

    if not present:
        dict_rs.get(name).append(0)
