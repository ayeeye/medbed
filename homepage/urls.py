from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('rekomendasi/', views.rekomendasi, name='rekomendasi'),
    path('detail/<str:image_url>/', views.detail, name='detail'),
    path('about/', views.about, name='about'),
    path('route/<slug:image_url>/', views.route, name='about'),
]
